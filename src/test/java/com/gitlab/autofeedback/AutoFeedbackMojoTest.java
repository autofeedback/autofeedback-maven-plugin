/*
 * Copyright 2020-2023 Aston University, University of York.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.gitlab.autofeedback;

import com.gitlab.autofeedback.api.CreateTokenResponse;
import com.gitlab.autofeedback.api.IServerConnector;
import com.gitlab.autofeedback.api.SubmissionResponse;
import com.gitlab.autofeedback.storage.TokenData;
import org.apache.hc.core5.http.HttpStatus;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.testing.MojoRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import javax.security.auth.login.LoginException;
import java.io.File;
import java.net.URL;
import java.sql.Date;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.zip.ZipFile;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class AutoFeedbackMojoTest
{
    private static final String PROJECT_PATH = "target/test-classes/project-to-test/";

    @Rule
    public MojoRule rule = new MojoRule()
    {
        @Override
        protected void before() throws Throwable
        {
        }

        @Override
        protected void after()
        {
        }
    };

    @Rule
    public TemporaryFolder tmpFolder = new TemporaryFolder();

    /**
     * @throws Exception if any
     */
    @Test
    public void testUpload() throws Exception {
        File pom = assertPomExists();
        UploadSubmissionMojo afMojo = getUploadSubmissionMojo(pom);
        IServerConnector mockConnector = setMockConnector(afMojo);
        afMojo.execute();
        assertTrue(afMojo.getDestinationFile().canRead());

        ZipFile zf = new ZipFile(afMojo.getDestinationFile());
        assertNotNull(zf.getEntry(".settings/some-file.txt"));
        assertNotNull(zf.getEntry("pom.xml"));
        assertNotNull(zf.getEntry("src/main/resources/random-file.txt"));
        zf.close();

        verify(mockConnector).createToken(afMojo.getServerUrl(), false);
        verify(mockConnector).uploadSubmission(any(TokenData.class), eq(afMojo.getServerUrl()), eq(afMojo.getAssessment()), eq(afMojo.getDestinationFile()), isNull());
    }

    @Test
    public void testUploadOnBehalfOf() throws Exception {
        File pom = assertPomExists();
        UploadSubmissionMojo afMojo = getUploadSubmissionMojo(pom);
        IServerConnector mockConnector = setMockConnector(afMojo);

        final String authorEmail = "foo@example.com";
        afMojo.setAuthorEmail(authorEmail);
        afMojo.execute();
        assertTrue(afMojo.getDestinationFile().canRead());

        verify(mockConnector).createToken(afMojo.getServerUrl(), false);
        verify(mockConnector).uploadSubmission(any(TokenData.class), eq(afMojo.getServerUrl()), eq(afMojo.getAssessment()), eq(afMojo.getDestinationFile()), eq(authorEmail));
    }

    @Test
    public void testFailedTokenCreation() throws Exception {
        File pom = assertPomExists();

        UploadSubmissionMojo afMojo = getUploadSubmissionMojo(pom);
        IServerConnector mockConnector = mock(IServerConnector.class);
        CreateTokenResponse sampleResponse = new CreateTokenResponse();
        sampleResponse.setTokenName(null);
        sampleResponse.setToken(null);
        sampleResponse.setExpiration(null);
        sampleResponse.setStatusCode(HttpStatus.SC_UNAUTHORIZED);
        when(mockConnector.createToken(any(URL.class), anyBoolean())).thenReturn(sampleResponse);
        afMojo.setConnector(mockConnector);

        try {
            afMojo.execute();
            fail("Build should have failed with a LoginException");
        } catch (MojoExecutionException ex) {
            verify(mockConnector, never()).uploadSubmission(any(TokenData.class), any(URL.class), anyInt(), any(File.class), isNull());
            assertTrue(ex.getCause() instanceof LoginException);
        }
    }

    private File assertPomExists() {
        File pom = new File(PROJECT_PATH);
        assertNotNull(pom);
        assertTrue(pom.exists());
        return pom;
    }

    private UploadSubmissionMojo getUploadSubmissionMojo(File pom) throws Exception {
        UploadSubmissionMojo afMojo = (UploadSubmissionMojo) rule.lookupConfiguredMojo(pom, UploadSubmissionMojo.GOAL_NAME);
        assertNotNull(afMojo);
        afMojo.setTokenStore(tmpFolder.newFile());
        return afMojo;
    }

    private IServerConnector setMockConnector(UploadSubmissionMojo afMojo) throws Exception {
        IServerConnector mockConnector = mock(IServerConnector.class);
        CreateTokenResponse sampleResponse = new CreateTokenResponse();
        sampleResponse.setTokenName("foo");
        sampleResponse.setToken("bar");
        sampleResponse.setExpiration(Date.from(Instant.now().plus(1, ChronoUnit.DAYS)));
        sampleResponse.setStatusCode(HttpStatus.SC_OK);
        assertNotNull(mockConnector);
        when(mockConnector.createToken(any(URL.class), anyBoolean())).thenReturn(sampleResponse);
        when(mockConnector.uploadSubmission(any(TokenData.class), any(URL.class), anyInt(), any(File.class), any())).thenReturn(new SubmissionResponse());
        afMojo.setConnector(mockConnector);
        return mockConnector;
    }

    /**
     * @throws Exception if any
     */
    @Test
    public void testPackage() throws Exception {
        File pom = assertPomExists();

        PackageSubmissionMojo afMojo = (PackageSubmissionMojo) rule.lookupConfiguredMojo(pom, PackageSubmissionMojo.GOAL_NAME);
        assertNotNull(afMojo);
        afMojo.execute();
        assertEquals("your-filename.zip", afMojo.getDestinationFile().getName());
        assertTrue(afMojo.getDestinationFile().canRead());

        ZipFile zf = new ZipFile(afMojo.getDestinationFile());
        assertNotNull(zf.getEntry(".settings/some-file.txt"));
        assertNotNull(zf.getEntry("pom.xml"));
        assertNotNull(zf.getEntry("src/main/resources/random-file.txt"));
        zf.close();
    }

}

