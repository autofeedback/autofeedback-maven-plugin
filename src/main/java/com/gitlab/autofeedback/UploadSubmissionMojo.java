/*
 * Copyright 2020-2023 Aston University, University of York.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.autofeedback;

import com.gitlab.autofeedback.api.CreateTokenResponse;
import com.gitlab.autofeedback.api.IServerConnector;
import com.gitlab.autofeedback.api.SubmissionResponse;
import com.gitlab.autofeedback.auth.ConsoleTokenRequestProvider;
import com.gitlab.autofeedback.auth.FormTokenRequestProvider;
import com.gitlab.autofeedback.gui.DialogUtil;
import com.gitlab.autofeedback.storage.TokenData;
import com.gitlab.autofeedback.storage.TokenStore;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.Map;
import javafx.application.Platform;
import javax.security.auth.login.LoginException;
import org.apache.hc.core5.http.HttpStatus;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

/**
 * Goal which compresses the source folders and POM and submits them to an AutoFeedback assessment.
 */
@Mojo(name = UploadSubmissionMojo.GOAL_NAME, defaultPhase = LifecyclePhase.NONE)
public class UploadSubmissionMojo extends AbstractAutoFeedbackMojo {

    public static final String GOAL_NAME = "uploadSubmission";

    /**
     * Path to the file with the token store.
     */
    @Parameter(defaultValue = "${user.home}/.autofeedback.json")
    private File tokenStore;

    /**
     * Email of the author of the submission, in case it is not the same as the submitter.
     * Must already be enrolled into the AutoFeedback module for the assessment.
     */
    @Parameter(property = "authorEmail")
    private String authorEmail;

    public File getTokenStore() {
        return tokenStore;
    }

    public void setTokenStore(File tokenStore) {
        this.tokenStore = tokenStore;
    }

    public String getAuthorEmail() {
        return authorEmail;
    }

    public void setAuthorEmail(String authorEmail) {
        this.authorEmail = authorEmail;
    }

    /**
     * Packs the submission and uploads it to the AutoFeedback server.
     */
    public void execute() throws MojoExecutionException {
        getLog().debug("Preparing submission to " + getServerUrl() + ", assessment " + getAssessment());
        try {
            packSubmission();

            IServerConnector connector = getConnector();
            connector.setLog(getLog());
            setupTokenRequestProvider();

            final TokenStore tokenStore = TokenStore.read(this.tokenStore);
            TokenData tokenData = tokenStore.getToken(getServerUrl());
            SubmissionResponse response = null;
            if (tokenData != null && tokenData.isValidNow()) {
                // If we had a valid token from before, try to use it
                response = connector.uploadSubmission(tokenData, getServerUrl(), getAssessment(),
                        destinationFile, authorEmail);
            }
            if (response == null || response.getUrl() == null) {
                // If we had no token from before, it expired, or it just didn't work, issue a new token and try again
                tokenData = issueToken(tokenStore, connector);
                response = connector.uploadSubmission(tokenData, getServerUrl(), getAssessment(),
                        destinationFile, authorEmail);
            }

            displayResponse(response);
        } catch (LoginException le) {
            SubmissionResponse response = new SubmissionResponse();
            response.setStatusCode(HttpStatus.SC_UNAUTHORIZED);
            response.setMessage(le.getMessage());
            displayResponse(response);

            throw new MojoExecutionException(le.getMessage(), le);
        } catch (Exception e) {
            throw new MojoExecutionException(e.getMessage(), e);
        }
    }

    private TokenData issueToken(TokenStore store, IServerConnector connector) throws Exception {
        final boolean reissue = store.getToken(getServerUrl()) != null;
        CreateTokenResponse response = connector.createToken(getServerUrl(), reissue);
        if (response.getStatusCode() != HttpStatus.SC_SUCCESS) {
            throw new LoginException(String.format(
                    "Failed to create token (error code %d): check your username and password.",
                    response.getStatusCode()));
        }

        TokenData tokenData = new TokenData();
        tokenData.setExpirationDate(response.getExpiration());
        tokenData.setTokenPlainText(response.getToken());
        tokenData.setTokenName(response.getTokenName());

        if (tokenData.getTokenPlainText() != null) {
            // Only save the token if we were successful
            store.putToken(getServerUrl(), tokenData);
            store.save(tokenStore, getLog());
        }

        return tokenData;
    }

    private void displayResponse(SubmissionResponse response) {
        if (isHeadless()) {
            if (response.getUrl() != null) {
                getLog().info(response.getMessage());
                getLog().info("URL: " + response.getUrl());
            } else {
                getLog().error(String.format(
                        "Unsuccessful result: %s (status code %d)", response.getMessage(), response.getStatusCode()));
                for (Map.Entry<String, List<String>> entry : response.getErrors().entrySet()) {
                    getLog().error(String.format(" * %s: %s", entry.getKey(), entry.getValue()));
                }
            }
        } else if (Desktop.isDesktopSupported() && response.getUrl() != null) {
            // Open in the default browser if possible
            try {
                Desktop.getDesktop().browse(URI.create(response.getUrl()));
            } catch (IOException e) {
                getLog().error("Could not open URI in the default browser: " + response.getUrl(), e);
            }
        } else {
            // Fall back on a JavaFX dialog
            try {
                DialogUtil.displayResponse(response);
            } catch (InterruptedException e) {
                getLog().error("Dialog interrupted: " + e.getMessage());
            } finally {
                Platform.exit();
            }
        }
    }

    private void setupTokenRequestProvider() {
        final IServerConnector connector = getConnector();

        if (isHeadless()) {
            getLog().debug("Using text input for username and password");
            connector.setTokenRequestProvider(new ConsoleTokenRequestProvider());
        } else {
            getLog().debug("Using GUI to request username and password");
            connector.setTokenRequestProvider(new FormTokenRequestProvider());
        }
    }

}
