/*
 * Copyright 2020-2023 Aston University, University of York.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.autofeedback.auth;

import com.gitlab.autofeedback.api.ITokenRequestProvider;
import com.gitlab.autofeedback.api.TokenRequest;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.concurrent.CountDownLatch;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * Token request provider which uses a JavaFX login form. This is not
 * thread-safe: only one may be used at a time.
 */
public class FormTokenRequestProvider implements ITokenRequestProvider {

    /**
     * Login form application. Uses a latch and static fields so other threads
     * can get to the email and password used.
     */
    public static class LoginForm extends Application {
        private static CountDownLatch latch = new CountDownLatch(1);

        private static String prompt;
        private static String tokenName;
        private static String email;
        private static String password;

        @FXML
        private TextField txtTokenName;

        @FXML
        private TextField txtEmail;

        @FXML
        private Label lblPrompt;

        @FXML
        private Label lblTokenName;

        @FXML
        private Label lblEmail;

        @FXML
        private Label lblPassword;

        @FXML
        private PasswordField txtPassword;

        @FXML
        private Button btnLogin;

        /**
         * Additional preparation after the components have been set up.
         * Associates labels with their components, and binds the login button to the fields being non-empty.
         */
        @FXML
        public void initialize() {
            if (prompt != null) {
                lblPrompt.setText(prompt);
            }

            lblTokenName.setLabelFor(txtTokenName);
            lblEmail.setLabelFor(txtEmail);
            lblPassword.setLabelFor(txtPassword);

            try {
                // Try to pre-fill token name with the machine hostname
                txtTokenName.setText(InetAddress.getLocalHost().getHostName());
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }

            btnLogin.disableProperty().bind(Bindings.or(
                    Bindings.isEmpty(txtTokenName.textProperty()),
                    Bindings.or(
                        Bindings.isEmpty(txtEmail.textProperty()),
                        Bindings.isEmpty(txtPassword.textProperty())
                    )));
        }

        /**
         * The Login button was clicked.
         */
        @FXML
        public void loginClicked() {
            saveCredentials(txtTokenName.getText(), txtEmail.getText(), txtPassword.getText());
            txtEmail.getScene().getWindow().hide();
        }

        /**
         * The Cancel button was clicked.
         */
        @FXML
        public void cancelClicked() {
            latch.countDown();
            txtEmail.getScene().getWindow().hide();
        }

        @Override
        public void start(Stage stage) throws Exception {
            Platform.setImplicitExit(false);

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(
                    getClass().getResource("/fxml/createTokenForm.fxml"));
            loader.setController(this);
            Parent parent = loader.load();

            stage.setTitle("Login form");
            stage.setResizable(false);
            stage.setScene(new Scene(parent));
            stage.show();
        }

        private static void reset() {
            email = null;
            password = null;
            tokenName = null;
            latch = new CountDownLatch(1);
        }

        private static void saveCredentials(String tokenName, String email, String password) {
            LoginForm.tokenName = tokenName;
            LoginForm.email = email;
            LoginForm.password = password;
            latch.countDown();
        }

        /**
         * Shows the form and returns a triplet of (tokenName, email, password) if Create Token
         * was clicked, or null if Cancel was clicked.
         */
        public static TokenRequest getTokenRequest() {
            try {
                latch.await();
                if (email != null) {
                    final TokenRequest request = new TokenRequest(tokenName, email, password);
                    reset();
                    return request;
                }
            } catch (InterruptedException e) {
                // Wait interrupted - do nothing
            }
            return null;
        }
    }

    private Thread threadJavaFX;

    @Override
    public TokenRequest getTokenRequest(boolean reissue) {
        if (threadJavaFX == null) {
            threadJavaFX = new Thread(() -> {
                final String fillPrompt = "Fill this form to create a personal access token:";
                if (reissue) {
                    LoginForm.prompt = "Your previous token is no longer valid. " + fillPrompt;
                } else {
                    LoginForm.prompt = "This is your first submission from this computer. " + fillPrompt;
                }
                Application.launch(LoginForm.class);
            });
            threadJavaFX.start();
        }

        return LoginForm.getTokenRequest();
    }

    public static void main(String[] args) {
        System.out.println(new FormTokenRequestProvider().getTokenRequest(false));
    }
}
