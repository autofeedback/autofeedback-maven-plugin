/*
 * Copyright 2020-2023 Aston University, University of York.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.autofeedback.api;

import java.util.Date;
import java.util.Objects;

public class CreateTokenResponse {
    private String tokenName;
    private String token;
    private Date expiration;
    private int statusCode;

    public String getTokenName() {
        return tokenName;
    }

    public void setTokenName(String tokenName) {
        this.tokenName = tokenName;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Date getExpiration() {
        return expiration != null ? new Date(expiration.getTime()) : null;
    }

    public void setExpiration(Date expiration) {
        this.expiration = expiration != null ? new Date(expiration.getTime()) : null;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CreateTokenResponse that = (CreateTokenResponse) o;
        return statusCode == that.statusCode
                && Objects.equals(token, that.token)
                && Objects.equals(expiration, that.expiration);
    }

    @Override
    public int hashCode() {
        return Objects.hash(token, expiration, statusCode);
    }

    @Override
    public String toString() {
        return "CreateTokenResponse{"
                + "token='" + token + '\''
                + ", expiration=" + expiration
                + ", statusCode=" + statusCode
                + '}';
    }
}
