/*
 * Copyright 2020-2023 Aston University, University of York.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.autofeedback.api;

import com.gitlab.autofeedback.storage.TokenData;
import java.io.File;
import java.net.URL;
import org.apache.maven.plugin.logging.Log;

/**
 * Represents a connection to a remote server.
 */
public interface IServerConnector {

    void setTokenRequestProvider(ITokenRequestProvider provider);

    void setLog(Log log);

    /**
     * Requests a new token from the server.
     * @param server URL to the server.
     * @param reissue {@code true} iff we are re-issuing a token,
     *                {@code false} if we are issuing a token for the first time.
     */
    CreateTokenResponse createToken(URL server, boolean reissue) throws Exception;

    /**
     * Uploads a zipped submission to the server.
     */
    SubmissionResponse uploadSubmission(TokenData token, URL server, int assessment,
                                        File zipFile, String authorEmail) throws Exception;

}
