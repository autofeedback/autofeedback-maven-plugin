/*
 * Copyright 2020-2023 Aston University, University of York.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.autofeedback.api;

import java.util.Objects;

public class TokenRequest {
    private final String tokenName;
    private final String username;
    private final String password;

    /**
     * Creates a new instance.
     */
    public TokenRequest(String tokenName, String username, String password) {
        this.tokenName = tokenName;
        this.username = username;
        this.password = password;
    }

    public String getTokenName() {
        return tokenName;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TokenRequest that = (TokenRequest) o;
        return Objects.equals(tokenName, that.tokenName)
                && Objects.equals(username, that.username)
                && Objects.equals(password, that.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tokenName, username, password);
    }

    @Override
    public String toString() {
        return "TokenRequest{"
                + "tokenName='" + tokenName + '\''
                + ", username='" + username + '\''
                + ", password='" + password + '\''
                + '}';
    }
}
