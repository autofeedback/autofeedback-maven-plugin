/*
 * Copyright 2020-2023 Aston University, University of York.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.autofeedback.api;

/**
 * Interface for a component that will provide the parameters needed to
 * request a token.
 */
public interface ITokenRequestProvider {

    /**
     * Provides a request object with three elements: the token name, the username, and the password.
     *
     * @param reissue <code>true</code> if we are reissuing the token, <code>false</code> otherwise.
     */
    TokenRequest getTokenRequest(boolean reissue);

}
