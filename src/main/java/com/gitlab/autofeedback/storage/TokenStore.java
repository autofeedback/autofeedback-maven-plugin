/*
 * Copyright 2020-2023 Aston University, University of York.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.autofeedback.storage;

import com.google.gson.Gson;
import java.io.*;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;
import org.apache.maven.plugin.logging.Log;

public class TokenStore {

    // Maps host + port to a token
    private final Map<String, TokenData> tokens = new HashMap<>();

    public TokenData getToken(URL url) {
        return tokens.get(getTokenKey(url));
    }

    private String getTokenKey(URL url) {
        return url.getHost() + ":" + url.getPort();
    }

    public void putToken(URL url, TokenData data) {
        tokens.put(getTokenKey(url), data);
    }

    public void clearToken(URL url) {
        tokens.remove(getTokenKey(url));
    }

    /**
     * Reads an instance from a JSON file.
     * @param file Source file.
     */
    public static TokenStore read(File file) throws IOException {
        if (!file.exists()) {
            return new TokenStore();
        } else {
            try (
                    FileReader fr = new FileReader(file, StandardCharsets.UTF_8);
                    BufferedReader br = new BufferedReader(fr)
            ) {
                final Gson gson = new Gson();
                final TokenStore tokenStore = gson.fromJson(br, TokenStore.class);
                return tokenStore != null ? tokenStore : new TokenStore();
            }
        }
    }

    /**
     * Writes the current instance into a file.
     * @param file Destination file.
     * @param log Maven log in case some permissions are not set.
     */
    public void save(File file, Log log) throws IOException {
        // Create an empty file if it does not exist, then change its permissions
        if (!file.exists()) {
            Files.createFile(file.toPath());
        }
        setStoreFilePermissions(file, log);

        // Now write the actual contents
        try (
                FileWriter fw = new FileWriter(file, StandardCharsets.UTF_8);
                BufferedWriter bw = new BufferedWriter(fw)
        ) {
            final Gson gson = new Gson();
            final String json = gson.toJson(this);
            bw.write(json);
        }
    }

    private void setStoreFilePermissions(File file, Log log) {
        // Not readable and writable in general
        boolean allPermsApplied = file.setReadable(false, false);
        allPermsApplied = file.setWritable(false, false) && allPermsApplied;

        // Readable and writable by the owner
        allPermsApplied = file.setReadable(true, true) && allPermsApplied;
        allPermsApplied = file.setWritable(true, true) && allPermsApplied;

        // Not executable
        allPermsApplied = file.setExecutable(false) && allPermsApplied;

        if (!allPermsApplied) {
            log.warn("Some of the file permissions on " + file + " were not set due to your OS."
                     + " Please double check your permissions and ensure that the file is not world-readable.");
        }
    }

}
