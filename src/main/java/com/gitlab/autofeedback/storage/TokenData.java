/*
 * Copyright 2020-2023 Aston University, University of York.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.autofeedback.storage;

import java.time.Instant;
import java.util.Date;
import java.util.Objects;

public class TokenData {
    private String tokenName;
    private String tokenPlainText;
    private Date expirationDate;

    public String getTokenName() {
        return tokenName;
    }

    public void setTokenName(String tokenName) {
        this.tokenName = tokenName;
    }

    public String getTokenPlainText() {
        return tokenPlainText;
    }

    public void setTokenPlainText(String tokenPlainText) {
        this.tokenPlainText = tokenPlainText;
    }

    public Date getExpirationDate() {
        return new Date(expirationDate.getTime());
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = new Date(expirationDate.getTime());
    }

    public boolean isValidNow() {
        return getExpirationDate().after(java.sql.Date.from(Instant.now()));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TokenData tokenData = (TokenData) o;
        return Objects.equals(tokenName, tokenData.tokenName)
                && Objects.equals(tokenPlainText, tokenData.tokenPlainText)
                && Objects.equals(expirationDate, tokenData.expirationDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tokenName, tokenPlainText, expirationDate);
    }
}
