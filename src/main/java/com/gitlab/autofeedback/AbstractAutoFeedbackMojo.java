/*
 * Copyright 2020-2023 Aston University, University of York.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.autofeedback;

import com.gitlab.autofeedback.api.IServerConnector;
import com.gitlab.autofeedback.connectors.HttpServerConnector;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Collections;
import java.util.Map;
import org.apache.maven.model.Resource;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.apache.maven.settings.Settings;
import org.apache.maven.shared.model.fileset.FileSet;
import org.apache.maven.shared.model.fileset.util.FileSetManager;
import org.codehaus.plexus.archiver.Archiver;

/**
 * Base class for AutoFeedback mojos.
 */
public abstract class AbstractAutoFeedbackMojo extends AbstractMojo {
    public static final String ARTIFACT_ID = "autofeedback-maven-plugin";
    public static final String GROUP_ID = "com.gitlab.autofeedback";

    /**
     * Maven project which is running this goal.
     */
    @Parameter(defaultValue = "${project}", readonly = true, required = true)
    protected MavenProject project;

    /**
     * The system settings for Maven. This is the instance resulting from
     * merging global and user-level settings files.
     */
    @Parameter(defaultValue = "${settings}", readonly = true, required = true)
    protected Settings settings;

    /**
     * ZIP file to be created.
     */
    @Parameter(defaultValue = "${project.basedir}/submission.zip")
    protected File destinationFile;

    @Component
    private Map<String, Archiver> archivers;

    /**
     * Base URL of the server, including scheme, host, and port (if non-standard).
     */
    @Parameter(required = true, property = "serverUrl")
    private URL serverUrl;

    /**
     * Identifier for the assessment to which the submission should be uploaded to.
     */
    @Parameter(required = true, property = "assessment")
    private int assessment;

    /**
     * If this is true and/or if we are in a headless mode, switches all prompts to be text-based.
     */
    @Parameter(property = "headless")
    private boolean headless;

    /**
     * Implements the connection to the server.
     */
    private IServerConnector connector = new HttpServerConnector();

    public File getDestinationFile() {
        return destinationFile;
    }

    public URL getServerUrl() {
        return serverUrl;
    }

    public IServerConnector getConnector() {
        return connector;
    }

    public void setConnector(IServerConnector connector) {
        this.connector = connector;
    }

    public int getAssessment() {
        return assessment;
    }

    public boolean isHeadless() {
        return headless || GraphicsEnvironment.isHeadless();
    }

    protected void packSubmission() throws IOException {
        Archiver zipArchiver = archivers.get("zip");
        zipArchiver.setDestFile(destinationFile);
        zipArchiver.setForced(true);
        getLog().debug("Generating ZIP file " + destinationFile);

        zipArchiver.addFile(project.getFile(), "pom.xml");
        getLog().debug("Added POM file " + project.getFile());

        for (String root : project.getCompileSourceRoots()) {
            getLog().debug("Going through files in compile source root " + root);
            addAllFiles(zipArchiver, root);
        }
        for (String root : project.getTestCompileSourceRoots()) {
            getLog().debug("Going through files in test compile source root " + root);
            addAllFiles(zipArchiver, root);
        }
        for (Resource r : project.getResources()) {
            getLog().debug("Going through resources in " + r.getDirectory());
            addAllFiles(zipArchiver, r.getDirectory());
        }
        for (Resource r : project.getTestResources()) {
            getLog().debug("Going through test resources in " + r.getDirectory());
            addAllFiles(zipArchiver, r.getDirectory());
        }

        File eclipseSettingsFolder = new File(project.getBasedir(), ".settings");
        if (eclipseSettingsFolder.isDirectory()) {
            getLog().debug("Adding Eclipse settings in " + eclipseSettingsFolder);
            addAllFiles(zipArchiver, eclipseSettingsFolder.toString());
        }

        zipArchiver.createArchive();
        this.getLog().info("Generated packed submission in " + destinationFile);
    }

    private void addAllFiles(Archiver zipArchiver, String root) {
        FileSetManager fileSetManager = new FileSetManager();
        FileSet sourceFileSet = new FileSet();
        sourceFileSet.setDirectory(root);
        sourceFileSet.setIncludes(Collections.singletonList("**/*"));

        for (String f : fileSetManager.getIncludedFiles(sourceFileSet)) {
            final File file = new File(root, f);
            String relativePath = project.getBasedir().toPath().relativize(file.toPath()).toString();
            zipArchiver.addFile(file, relativePath);
            getLog().debug("Added " + f);
        }
    }
}
