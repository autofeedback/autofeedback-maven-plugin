/*
 * Copyright 2020-2023 Aston University, University of York.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.autofeedback.gui;

import com.gitlab.autofeedback.api.SubmissionResponse;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.stage.Stage;

public class DialogUtil {

    /**
     * Runs a one-shot JavaFX application showing the response from the server.
     */
    public static void displayResponse(SubmissionResponse response) throws InterruptedException {
        DialogApplication.latch = new CountDownLatch(1);
        DialogApplication.response = response;
        new Thread(() -> {
            try {
                // This may be the only JavaFX application run so far
                Application.launch(DialogApplication.class);
            } catch (IllegalStateException ex) {
                // An application may have been run already (e.g. login form)
                Platform.runLater(() -> {
                    try {
                        new DialogApplication().start(null);
                    } catch (Exception e) {
                        e.printStackTrace();
                        DialogApplication.latch.countDown();
                    }
                });
            }
        }).start();
        DialogApplication.latch.await();
    }

    /**
     * Test program for internal development. Shows the response dialog for an empty response.
     */
    public static void main(String[] args) {
        try {
            displayResponse(new SubmissionResponse());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static class DialogApplication extends Application {
        private static CountDownLatch latch;
        private static SubmissionResponse response;

        @Override
        public void start(Stage stage) throws Exception {
            Alert alert = new Alert(response.getUrl() != null ? Alert.AlertType.INFORMATION : Alert.AlertType.ERROR);
            alert.setTitle("AutoFeedback Maven plugin");
            alert.setHeaderText(response.getMessage());
            if (response.getUrl() == null) {
                if (!response.getErrors().isEmpty()) {
                    StringBuilder sb = new StringBuilder("Errors:");
                    sb.append(System.lineSeparator());
                    for (Map.Entry<String, List<String>> entry : response.getErrors().entrySet()) {
                        sb.append("* ");
                        sb.append(entry.getKey());
                        sb.append(" ");
                        sb.append(entry.getValue());
                    }
                    alert.setContentText(sb.toString());
                }
            } else {
                alert.setContentText("Check the results here:" + System.lineSeparator() + response.getUrl());
            }
            alert.showAndWait();
            latch.countDown();
        }
    }
}
