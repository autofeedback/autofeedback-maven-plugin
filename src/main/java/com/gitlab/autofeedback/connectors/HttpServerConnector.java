/*
 * Copyright 2020-2023 Aston University, University of York.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.autofeedback.connectors;

import com.gitlab.autofeedback.api.*;
import com.gitlab.autofeedback.storage.TokenData;
import com.google.gson.Gson;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.apache.hc.client5.http.auth.AuthScope;
import org.apache.hc.client5.http.auth.UsernamePasswordCredentials;
import org.apache.hc.client5.http.classic.methods.HttpPost;
import org.apache.hc.client5.http.entity.UrlEncodedFormEntity;
import org.apache.hc.client5.http.entity.mime.FileBody;
import org.apache.hc.client5.http.entity.mime.MultipartEntityBuilder;
import org.apache.hc.client5.http.entity.mime.StringBody;
import org.apache.hc.client5.http.impl.auth.BasicCredentialsProvider;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.CloseableHttpResponse;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.core5.http.*;
import org.apache.hc.core5.http.io.entity.EntityUtils;
import org.apache.hc.core5.http.message.BasicNameValuePair;
import org.apache.maven.plugin.logging.Log;

/**
 * Connects to an AutoFeedback server through HTTP, using Basic auth to create the token and
 * token auth for the rest of the endpoints.
 */
public class HttpServerConnector implements IServerConnector {

    private ITokenRequestProvider authProvider;
    private Log log;

    @Override
    public void setTokenRequestProvider(ITokenRequestProvider provider) {
        this.authProvider = provider;
    }

    @Override
    public void setLog(Log log) {
        this.log = log;
    }

    @Override
    public CreateTokenResponse createToken(URL server, boolean reissue) throws Exception {
        if (authProvider == null) {
            throw new IllegalStateException("A token request provider needs to be set in order to use createToken.");
        }

        final TokenRequest tokenRequest = authProvider.getTokenRequest(reissue);
        final URI postUri = server.toURI().resolve("/api/tokens/create");
        final BasicCredentialsProvider credentialsProvider = new BasicCredentialsProvider();
        final HttpHost host = new HttpHost(postUri.getScheme(), postUri.getHost(), postUri.getPort());
        if (tokenRequest != null) {
            final UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(
                    tokenRequest.getUsername(), tokenRequest.getPassword().toCharArray());
            credentialsProvider.setCredentials(new AuthScope(host), credentials);
        } else {
            throw new IllegalStateException("The token request was cancelled.");
        }

        try (CloseableHttpClient client = HttpClients.custom()
                .setDefaultCredentialsProvider(credentialsProvider).build()) {
            HttpPost httpPost = new HttpPost(postUri.toString());
            httpPost.setHeader("X-Requested-With", "XMLHttpRequest");

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("tokenName", tokenRequest.getTokenName()));
            httpPost.setEntity(new UrlEncodedFormEntity(params));

            try (CloseableHttpResponse response = client.execute(httpPost)) {
                logSuccessOrFailure(postUri, response);

                final String responseText = EntityUtils.toString(response.getEntity());
                Gson gson = new Gson();
                CreateTokenResponse serverResponse = gson.fromJson(responseText, CreateTokenResponse.class);
                serverResponse.setTokenName(tokenRequest.getTokenName());
                serverResponse.setStatusCode(response.getCode());

                return serverResponse;
            }
        }
    }

    @Override
    public SubmissionResponse uploadSubmission(
            TokenData token, URL server, int assessment, File zipFile, String authorEmail)
            throws URISyntaxException, IOException, ParseException {
        final String path = String.format("/api/assessments/%d/storeSubmission", assessment);
        final URI postUri = server.toURI().resolve(path);
        log.info("Submitting to " + postUri + (authorEmail == null ? "" : " on behalf of " + authorEmail));

        try (CloseableHttpClient client = HttpClients.createDefault()) {
            HttpPost httpPost = new HttpPost(postUri.toString());
            httpPost.setHeader("X-Requested-With", "XMLHttpRequest");
            httpPost.setHeader("Authorization", "Bearer " + token.getTokenPlainText());

            final FileBody bin = new FileBody(zipFile);
            final MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            if (authorEmail != null) {
                builder.addPart("authorEmail", new StringBody(authorEmail, ContentType.MULTIPART_FORM_DATA));
            }
            builder.addPart("jobfile", bin);
            httpPost.setEntity(builder.build());

            try (CloseableHttpResponse response = client.execute(httpPost)) {
                logSuccessOrFailure(postUri, response);

                final String responseText = EntityUtils.toString(response.getEntity());
                Gson gson = new Gson();
                SubmissionResponse serverResponse = gson.fromJson(responseText, SubmissionResponse.class);
                serverResponse.setStatusCode(response.getCode());

                return serverResponse;
            }
        }
    }

    private void logSuccessOrFailure(URI postUri, CloseableHttpResponse response) {
        if (response.getCode() >= HttpStatus.SC_SUCCESS && response.getCode() < HttpStatus.SC_REDIRECTION) {
            log.debug("Response code from " + postUri + " indicates success: " + response.getCode());
        } else {
            log.error("Response code from " + postUri + " indicates failure: " + response.getCode());
        }
    }
}
