# AutoFeedback Maven Plugin

## Prerequisites

* [Apache Maven](http://maven.apache.org/) 3.x or later.
* [Java](http://java.oracle.com/) 11 or later.

## Recommended IDEs

To work on this project, a Maven-aware IDE is recommended.
[IntelliJ](https://www.jetbrains.com/idea/) has native Maven integration, and Eclipse supports Maven through the [M2Eclipse](https://www.eclipse.org/m2e/) project.

IntelliJ in general requires less setup to get start with this project.
Simply open the root folder of this repository as a project.

## Building the tool

This will build the plugin, run the basic unit tests, and install it into the local Maven repository:

```sh
mvn install
```

## Integration tests

To run the integration tests, you will need to have the [AutoFeedback webapp](https://gitlab.com/docker-autofeedback/webapp) running locally, having set up an assessment with ID #2.
Alternatively, you could change the `src/it` POM files to point at a live AutoFeedback server, replacing the assessment ID accordingly.
You will need to enable the `run-its` profile:

```sh
mvn -Prun-its integration-test
```

The integration tests assume that the default root user is available in the server (`root@example.com`), and that an assessment with ID 1 is available.
If this is not the case, change `src/it/settings.xml` accordingly.

## Gitlab CI

The `.gitlab-ci.yml` script automatically tests and deploys the plugin to the Gitlab package registry.
The `master` branch produces SNAPSHOT versions in the Gitlab package repository.

To produce non-SNAPSHOT versions, push a tag with the desired version number (e.g. `0.2.0`) and Gitlab will automatically call `mvn versions:set` as needed before deploying.

## Git hooks

The repository comes with a number of hooks to run linters between commits, and to run tests before pushing.
To set up these hooks in Linux/Mac, run `bin/create-hook-symlinks`.

For the Markdown linter, you will need to install `mdl`:

```sh
gem install mdl
```
