# AutoFeedback Maven plugin

This project contains the sources for an [Apache Maven](http://maven.apache.org/) plugin to be used with the [AutoFeedback](https://gitlab.com/docker-autofeedback/autofeedback-webapp) web-based code feedback platform.

This plugin requires Maven 3.6.0 or newer.

## Adding the plugin to the project

Add the plugin to your `<build>` section.
A minimal configuration of this plugin is as follows:

```xml
<plugin>
  <groupId>com.gitlab.autofeedback</groupId>
  <artifactId>autofeedback-maven-plugin</artifactId>
  <version>0.5.2</version>
  <configuration>
    <serverUrl>http://localhost:3000</serverUrl>
    <assessment>2</assessment>
  </configuration>
</plugin>
```

`serverUrl` should be the base URL of the AutoFeedback server, and `assessment` should be the ID of the assessment to which the submission should be uploaded.
You can obtain the assessment ID from the URL of the `Model solution` page of the `assessment`, if you have the appropriate permissions.
In the example above, it will look like this:

```txt
http://localhost:3000/modules/1/assessments/2/solution
```

## Goals

The plugin provides the following goals:

### packageSubmsision

This goal zips all the compile and test sources and resources, the POM file itself, and the Eclipse JDT settings.
The ZIP file will be called `submission.zip` by default.

### uploadSubmission

This goal zips all the relevant files (see `packageSubmission` above), and then uploads the file to the AutoFeedback platform.
The goal can run in "headless" mode (where a graphical environment is not available), and in GUI mode (using JavaFX).

The plugin will check in `$HOME/.autofeedback.json` if there is a personal access token for that AutoFeedback server (based on host + port).
If there are no tokens for it, it will ask for the token name and AF username/password (through the console in headless mode, or through a JavaFX form otherwise).
When using the JavaFX form, the token name will be prefilled with the machine hostname (which should be enough in most cases).

The result of the upload will be shown in the console if running in "headless" mode, in the browser if desktop capabilities are available, or in a JavaFX dialog otherwise.

## Typical usage

Generate a ZIP to submit to Blackboard or AutoFeedback with:

```sh
mvn autofeedback:packageSubmission
```

Generate the ZIP and submit it to AutoFeedback with:

```sh
mvn autofeedback:uploadSubmission
```

If you are using Windows and have issues submitting, you may need to specify an additional VM flag (issue is documented [here](https://openjfx.io/openjfx-docs/#IDE-Eclipse)):

```sh
MAVEN_OPTS="-Djava.library.path=C:\tmp" mvn autofeedback:uploadSubmission
```

### Headless mode

If you want to only use text entry and output, set `headless` to `true` in your POM file:

```xml
<configuration>
  <serverUrl>http://localhost:3000</serverUrl>
  <assessment>2</assessment>
  <headless>true</headless>
</configuration>
```

You can also specify the option from the command line, without changing the POM:

```sh
mvn autofeedback:uploadSubmission -Dheadless
```

### Submitting on behalf of a student

If you are a module tutor and you want to submit on behalf of a student, use the `-DauthorEmail` option from their unpacked submission:

```shell
mvn autofeedback:uploadSubmission -DauthorEmail=student@example.com
```

The student will need to be already enrolled in the module.

### Customising the name of the generated ZIP file

In order to customise the name of the ZIP file, use `destinationFile`:

```xml
<configuration>
  <serverUrl>http://localhost:3000</serverUrl>
  <assessment>2</assessment>
  <destinationFile>${project.basedir}/your-filename.zip</destinationFile>
</configuration>
```
